/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.r5design.difffinder;

import ij.IJ;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.xml.XMLManager;
import org.json.simple.parser.ParseException;

/**
 *
 * @author R5
 */
public class ProcessorMutations {

    final static int POPULATION_SIZE=50;
    final static int MAX_EVOLUTIONS=1000;
    final static int SAME_FITNESS_MAX=15;
    final static String PICS_PATH="D:\\diffpics\\";
    final static String OUTPUT_PATH="A:\\output\\";
    
    public static Configuration buildEvConf () throws InvalidConfigurationException, IOException, ParseException, SQLException, ClassNotFoundException {
        Configuration conf = new DefaultConfiguration();
        conf.setPopulationSize( POPULATION_SIZE );
        ProcessorFitnessFunction ff = new ProcessorFitnessFunction(PICS_PATH,OUTPUT_PATH);
        conf.setFitnessFunction(ff);
        return conf;
    }
    
    public static void main(String[] args) throws IOException, ParseException, InvalidConfigurationException, FileNotFoundException, SQLException {
        //TestImages.loadImages(DBOperator.getInstance());
        //int d=5;
        try {
            Configuration conf = buildEvConf();
            ProcessorGenes pG = ProcessorGenes.getInstance();
            Chromosome pS = pG.getExampleChromosome(conf);
            conf.setSampleChromosome( pS );
            Genotype population;
            try {
                population=XMLManager.getGenotypeFromDocument(conf, XMLManager.readFile(new File(OUTPUT_PATH+"population.dat")));
            }
            catch (FileNotFoundException e) {
                population = Genotype.randomInitialGenotype(conf );
            }
            IChromosome bestSolutionSoFar=population.getFittestChromosome();
            ProcessorFitnessFunction pFF = (ProcessorFitnessFunction) conf.getFitnessFunction();
            TestImages tI = TestImages.getInstance();
            //tI.addCandidate(pFF, bestSolutionSoFar);
            
          /*
            DBOperator dbOp = DBOperator.getInstance().setConnection("jdbc:sqlite:D:\\diffFinder\\testing\\ImageData2.db");
        for (int i=0; i<tI.allImages.size(); i++) {
                if (!pFF.testImage(bestSolutionSoFar,tI.allImages.get(i))) {
                    for (Rectangle r : tI.allImagesRects.get(tI.allImages.get(i))) {
                        dbOp.insertImageRect(r, tI.allImages.get(i));
                    }
                }
                //System.out.println(tI.allImages.get(i)+" : "+pFF.testImage(bestSolutionSoFar, tI.allImages.get(i)));
            }
        if (true) {
            return;
        }
        */
        
            do {
                double currFit=0, prevFit=0;
                int sameFit=0;
                for (int i=0; sameFit<SAME_FITNESS_MAX && i<MAX_EVOLUTIONS; i++) {
                    prevFit = currFit;
                    population.evolve();
                    bestSolutionSoFar = population.getFittestChromosome();
                    currFit = pFF.evaluate(bestSolutionSoFar);
                    if (currFit==prevFit) {
                        sameFit++;
                    }
                    else {
                        sameFit=0;
                    }
                    //Я понятия не имею, что происходит в ImageJ и меня это беспокоит.
                    IJ.run("Close All", "");
                    IJ.freeMemory();
                    XMLManager.writeFile(XMLManager.representGenotypeAsDocument(population), new File(OUTPUT_PATH+"population.dat"));
                    System.out.println();
                    System.out.println("----------------------");
                    System.out.println("Epoch #"+(i+1)+
                            ": best fitness = "+currFit+
                            ", same solution = "+sameFit+"/"+SAME_FITNESS_MAX+
                            ", pictures used = "+tI.activeImages.size()+"/"+tI.allImages.size()
                    );
                    System.out.println("----------------------");
                }
                for (int i=0; i<tI.allImages.size(); i++) {
                    System.out.println(tI.allImages.get(i)+" - "+pFF.testImage(bestSolutionSoFar, tI.allImages.get(i)));
                }
                //pFF.flushFitnessCache();
            }
            while (tI.addCandidate(pFF, bestSolutionSoFar) != false);
            /*
            for (int i=0; i<tI.allImages.size(); i++) {
                pFF.markImagePoints(outputBasePath, tI.allImages.get(i), tI.allImagesFiles.get(tI.allImages.get(i)), bestSolutionSoFar);
                System.out.println(tI.allImages.get(i)+" : "+pFF.testImage(bestSolutionSoFar, tI.allImages.get(i)));
            }
            */
            
            for (int i=0; i<tI.allImages.size(); i++) {
                if (pFF.testImage(bestSolutionSoFar,tI.allImages.get(i))) {
                    pFF.markImagePoints(OUTPUT_PATH+"solved/", tI.allImages.get(i), tI.allImagesFiles.get(tI.allImages.get(i))[0], tI.allImagesFiles.get(tI.allImages.get(i))[1], bestSolutionSoFar);
                }
                else {
                    pFF.markImagePoints(OUTPUT_PATH+"failed/", tI.allImages.get(i), tI.allImagesFiles.get(tI.allImages.get(i))[0], tI.allImagesFiles.get(tI.allImages.get(i))[1], bestSolutionSoFar);
                }
                //System.out.println(tI.allImages.get(i)+" : "+pFF.testImage(bestSolutionSoFar, tI.allImages.get(i)));
            }
            
            for (int i=0; i<tI.activeImages.size(); i++) {
                pFF.markImagePoints(OUTPUT_PATH, tI.activeImages.get(i), tI.allImagesFiles.get(tI.activeImages.get(i))[0], tI.allImagesFiles.get(tI.activeImages.get(i))[0], bestSolutionSoFar);
                System.out.println(tI.activeImages.get(i)+" : "+pFF.testImage(bestSolutionSoFar, tI.activeImages.get(i)));
            }
            
            pFF.serializeGenes(OUTPUT_PATH+"genes.json", bestSolutionSoFar);
            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
