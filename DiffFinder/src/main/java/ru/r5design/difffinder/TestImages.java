/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.r5design.difffinder;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonObject;
import ij.IJ;
import ij.ImagePlus;
import ij.plugin.ImageCalculator;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.jgap.IChromosome;
import static ru.r5design.difffinder.ProcessorGenes.INSTANCE;

/**
 *
 * @author R5
 */
enum TestImages {
    INSTANCE;
    
    final DBOperator dbOp = DBOperator.getInstance();
    
    //Берём в activeImages лишь те, которые не прошли тест
    HashMap<String,ArrayList<Rectangle>> allImagesRects = new HashMap<>();
    HashMap<String,BufferedImage[]> allImagesFiles = new HashMap<>();
    ArrayList<String> allImages = new ArrayList<>();
    ArrayList<String> activeImages = new ArrayList<>();
    
    public static void loadImages(DBOperator dbOp) throws FileNotFoundException, SQLException, IOException {
        JsonParser parser = new JsonParser();
        BufferedReader br = new BufferedReader(
                new FileReader("D:\\diffpics\\diffs.json")
        );
        JsonElement jsonElement = parser.parse(br);
        JsonObject list = jsonElement.getAsJsonObject();
        JsonArray imgs = list.getAsJsonArray("list");
        for (JsonElement img : imgs) {
            //System.out.println(img.toString());
            JsonArray rects = img.getAsJsonObject().get("diffs").getAsJsonArray();
            String baseName = img.getAsJsonObject().get("name").getAsString();
            for (JsonElement rect : rects) {
                JsonObject rectObj = rect.getAsJsonObject();
                Rectangle r = new Rectangle(rectObj.get("x").getAsInt(),rectObj.get("y").getAsInt(),rectObj.get("w").getAsInt(),rectObj.get("h").getAsInt());
                //System.out.println(r.toString());
                dbOp.insertImageRect(r, baseName);
            }
            //dbOp.insertImageRect(rect, imgBaseName);
        }
    }
    
    /**
    * Converts a given Image into a BufferedImage
    *
    * @param img The Image to be converted
    * @return The converted BufferedImage
    */
   public static BufferedImage toBufferedImage(Image img)
   {
       if (img instanceof BufferedImage)
       {
           return (BufferedImage) img;
       }

       // Create a buffered image with transparency
       BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

       // Draw the image on to the buffered image
       Graphics2D bGr = bimage.createGraphics();
       bGr.drawImage(img, 0, 0, null);
       bGr.dispose();

       // Return the buffered image
       return bimage;
   }
    
    void loadFiles() throws IOException {
        BufferedImage a,b;
        ImagePlus img1, img2;
        final String basePath = "D:\\diffpics\\";
        final ImageCalculator ic = new ImageCalculator();
        for (String image : allImages) {
            a = ImageIO.read(new File(basePath + image + "_1.png"));
            b = ImageIO.read(new File(basePath + image + "_2.png"));
            //img1 = new ImagePlus("first", a);
            //img2 = new ImagePlus("second", b);
            //img2 = ic.run("Difference create", img1, img2);
            //allImagesFiles.put(image, toBufferedImage(img2.getImage()));
            allImagesFiles.put(image, new BufferedImage[] {a,b});
        }
    }
    
    Random rnd = new Random();
    TestImages() {
        try {
            dbOp.getImgsRects(allImages, allImagesRects);
            java.util.Collections.shuffle(allImages);
            loadFiles();
        }
        catch (Exception e) {
            System.out.println("DANGER! DANGER! HIGH VOLTAGE!!!");
        }
        //Check&add
        //activeImages.add(allImages.get(0));
        try {
            activeImages.add(allImages.get(rnd.nextInt(allImages.size())));
        }
        catch (java.lang.IllegalArgumentException ex) {
            Logger.getLogger(TestImages.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Adds new candidate image to testing set
    public boolean addCandidate (ProcessorFitnessFunction pFF, IChromosome ic) {
        if (activeImages.size() == allImages.size()) {
            return false;
        }
        //if (activeImages.size() == 0) {
        //    activeImages.add(allImages.get(0));
        //}
        for (int i=0; i<allImages.size(); i++) {
            if (!activeImages.contains(allImages.get(i)) && !pFF.testImage(ic, allImages.get(i))) {
                activeImages.add(allImages.get(i));
                return true;
            }
        }
        return false;
    }
    
    // Static getter
    public static TestImages getInstance()
    {
        return INSTANCE;
    }
}
