/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.r5design.difffinder;

import java.util.Random;
import org.jgap.Gene;

/**
 *
 * @author R5
 */
public class ProcessorAnnealing {
    double START_TEMP = 10;
    double MIN_TEMP = 0.01;
    int ITERS = 1000;
    
    double eval(double v[]) {
        return v[0]*3-v[1];
    }
    
    Random r = new Random();
    void generatePoint(double v[]) {
        v[0] = 10*r.nextDouble();
        v[1] = 5+5*r.nextDouble();
    }
    
    double decreaseTemperature(int k) {
        return (START_TEMP-MIN_TEMP)*((ITERS-k)/((double)ITERS))+MIN_TEMP;
    }
    
    Gene[] anneal() {
        double temp = START_TEMP;
        double v[] = new double[2];
        for (int i=0; i<ITERS; i++) {
            System.out.println(temp);
            temp = decreaseTemperature(i);
        }
        System.out.println(temp);
        return null;
    }
    
    public static void main2(String[] args) {
        ProcessorAnnealing pa = new ProcessorAnnealing();
        pa.anneal();
    }
}
