/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.r5design.difffinder;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

enum DBOperator
{
    INSTANCE;
    
    Connection connection;
    Connection casesConnection;

    DBOperator()
    {
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:C:\\diffbot\\stuff\\ImageData.db");
            casesConnection = DriverManager.getConnection("jdbc:sqlite:C:\\diffbot\\stuff\\ImageCases.db");
        } catch (SQLException ex) {
            Logger.getLogger(DBOperator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    DBOperator setConnection(String connStr) throws SQLException {
        connection.close();
        connection = DriverManager.getConnection(connStr);
        return getInstance();
    }
    
   /** Read the object from Base64 string. */
   private static Object fromString( String s ) throws IOException ,
                                                       ClassNotFoundException {
        byte [] data = Base64.getDecoder().decode( s );
        ObjectInputStream ois = new ObjectInputStream( 
                                        new ByteArrayInputStream(  data ) );
        Object o  = ois.readObject();
        ois.close();
        return o;
   }

    /** Write the object to a Base64 string. */
    private static String toString( Serializable o ) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject( o );
        oos.close();
        return Base64.getEncoder().encodeToString(baos.toByteArray()); 
    }
    
    public void insertImageRect (Rectangle rect, String imgBaseName) throws SQLException, IOException {
        String rectString = (rect.x+":"+rect.y+":"+rect.width+":"+rect.height);
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        //Пока без индексов
        String sql = "CREATE TABLE IF NOT EXISTS rects " +
                   "(basename   VARCHAR(32)     NOT NULL," +
                   "rectParams  NVARCHAR(64) NOT NULL," +
                   " rect           BLOB    NOT NULL)"; 
        statement.executeUpdate(sql);
        String rectObjString = DBOperator.toString(rect);
        int count = statement.executeQuery("SELECT COUNT(1) FROM rects WHERE basename=\'"+imgBaseName+"\' AND rectParams=\'"+rectString+"\'").getInt(1);
        if (count==0) {
            sql = "INSERT INTO rects (basename, rectParams, rect) VALUES (\'"+imgBaseName+"\',\'"+rectString+"\',\'"+rectObjString+"\')";
            statement.executeUpdate(sql);
        }
        statement.close();
    }
    
    public void getImgsRects(ArrayList<String> imgsBaseNames, HashMap<String, ArrayList<Rectangle>> imgsRects) throws SQLException, IOException, ClassNotFoundException {
        PreparedStatement pS = connection.prepareStatement("SELECT basename, rectParams, rect FROM rects");
        ResultSet rs=pS.executeQuery();
        while (rs.next())
        {
            String baseName = rs.getString(1);
            if (!imgsBaseNames.contains(baseName)) {
                imgsBaseNames.add(baseName);
                if (!imgsRects.containsKey(baseName)) {
                    imgsRects.put(baseName, new ArrayList<>());
                }
            }
            Rectangle rect = (Rectangle)(fromString(rs.getString(3)));
            imgsRects.get(baseName).add(rect);
        }
        rs.close();
        pS.close();
    }

    public static DBOperator getInstance()
    {
        return INSTANCE;
    }
    
    
}