/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.r5design.difffinder;

import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.gui.OvalRoi;
import ij.io.Opener;
import ij.plugin.ChannelSplitter;
import ij.plugin.ImageCalculator;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author R5
 */
public final class Comparer {

    private static final boolean printDebugInfo = true;

    private static ArrayList<Integer[]> deletePointsByDistance(ArrayList<Integer[]> points, ImagePlus dilatedImg, int pCount) {
        while (points.size() > pCount) {
            double slope = 0, intercept;
            boolean fixedX;
            int k, v, kMax, kMin, direction;
            int tX1 = 0, tX2 = 0, tY1 = 0, tY2 = 0;
            int pX1, pY1, pX2, pY2;
            double currDistance;
            boolean stopSearch;
            ArrayList<Integer> pairsFirst = new ArrayList<>(), pairsSecond = new ArrayList<>();
            ArrayList<Double> pairDistanceValues = new ArrayList<>();
            ArrayList<Double> distanceValues = new ArrayList<>();
            double minDVal;
            int minPairP1Index = 0, minPairP2Index = 0, deletedIndex;
            for (int i = 0; i < points.size(); i++) {
                distanceValues.add(0.0);
            }
            for (int i = 0; i < points.size() - 1; i++) {
                for (int j = i + 1; j < points.size(); j++) {
                    fixedX = false;
                    try {
                        slope = ((double) points.get(j)[1] - points.get(i)[1]) / ((double) points.get(j)[0] - points.get(i)[0]);
                    } catch (ArithmeticException e) {
                        fixedX = true;
                    }
                    intercept = (double) points.get(i)[1] - slope * points.get(i)[0];
                    stopSearch = false;
                    for (k = points.get(i)[1], kMax = points.get(j)[1], direction = ((points.get(i)[1] < points.get(j)[1]) ? 1 : (-1)); !stopSearch && k != kMax; k += direction) {
                        v = (int) (fixedX ? points.get(i)[0] : ((k - intercept) / slope));
                        if (dilatedImg.getPixel(v, k)[0] != 255) {
                            tX1 = v;
                            tY1 = k;
                            stopSearch = true;
                        }
                    }
                    stopSearch = false;
                    for (k = points.get(i)[0], kMax = points.get(j)[0], direction = ((points.get(i)[0] < points.get(j)[0]) ? 1 : (-1)); !stopSearch && k != kMax; k += direction) {
                        v = (int) (intercept + slope * k);
                        if (dilatedImg.getPixel(k, v)[0] != 255) {
                            tX2 = k;
                            tY2 = v;
                            stopSearch = true;
                        }
                    }
                    if (Point2D.distance(points.get(i)[0], points.get(i)[1], tX1, tY1) > Point2D.distance(points.get(i)[0], points.get(i)[1], tX2, tY2)) {
                        pX1 = tX2;
                        pY1 = tY2;
                    } else {
                        pX1 = tX1;
                        pY1 = tY1;
                    }
                    stopSearch = false;
                    for (k = points.get(j)[1], kMax = points.get(i)[1], direction = ((points.get(j)[1] < points.get(i)[1]) ? 1 : (-1)); !stopSearch && k != kMax; k += direction) {
                        v = (int) (fixedX ? points.get(j)[0] : ((k - intercept) / slope));
                        if (dilatedImg.getPixel(v, k)[0] != 255) {
                            tX1 = v;
                            tY1 = k;
                            stopSearch = true;
                        }
                    }
                    stopSearch = false;
                    for (k = points.get(j)[0], kMax = points.get(i)[0], direction = ((points.get(j)[0] < points.get(i)[0]) ? 1 : (-1)); !stopSearch && k != kMax; k += direction) {
                        v = (int) (intercept + slope * k);
                        if (dilatedImg.getPixel(v, k)[0] != 255) {
                            tX2 = v;
                            tY2 = k;
                            stopSearch = true;
                        }
                    }
                    if (Point2D.distance(points.get(j)[0], points.get(j)[1], tX1, tY1) > Point2D.distance(points.get(j)[0], points.get(j)[1], tX2, tY2)) {
                        pX2 = tX2;
                        pY2 = tY2;
                    } else {
                        pX2 = tX1;
                        pY2 = tY1;
                    }
                    pairsFirst.add(i);
                    pairsSecond.add(j);
                    currDistance = Point2D.distance(pX1, pY1, pX2, pY2);
                    pairDistanceValues.add(currDistance);
                    distanceValues.set(i, distanceValues.get(i) + currDistance);
                    distanceValues.set(j, distanceValues.get(j) + currDistance);
                }
            }
            minDVal = Double.MAX_VALUE;
            for (int i = 0; i < pairDistanceValues.size(); i++) {
                if (minDVal > pairDistanceValues.get(i)) {
                    minDVal = pairDistanceValues.get(i);
                    minPairP1Index = pairsFirst.get(i);
                    minPairP2Index = pairsSecond.get(i);
                }
            }
            if (distanceValues.get(minPairP1Index) > distanceValues.get(minPairP2Index)) {
                deletedIndex = minPairP2Index;
                //points.get(minPairP1Index)[0] = (int) (points.get(minPairP1Index)[0]*(0.4)+points.get(minPairP2Index)[0]*(0.6));
                //points.get(minPairP1Index)[1] = (int) (points.get(minPairP1Index)[1]*(0.4)+points.get(minPairP2Index)[1]*(0.6));
            } else {
                deletedIndex = minPairP1Index;
                //points.get(minPairP2Index)[0] = (int) (points.get(minPairP2Index)[0]*(0.4)+points.get(minPairP1Index)[0]*(0.6));
                //points.get(minPairP2Index)[1] = (int) (points.get(minPairP2Index)[1]*(0.4)+points.get(minPairP1Index)[1]*(0.6));
            }
            points.remove(deletedIndex);
            distanceValues.remove(deletedIndex);
        }
        return points;
    }
    
    private static ArrayList<Integer[]> deletePointsByDiversity(ArrayList<Integer[]> points, ImagePlus diffImg, int pCount) {
        while (points.size() > pCount) {
            double slope = 0, intercept;
            boolean fixedX;
            int k, v, kMax, kMin, direction;
            //int tX1 = 0, tX2 = 0, tY1 = 0, tY2 = 0;
            //int pX1, pY1, pX2, pY2;
            int topDiv, bottomDiv;
            double currDistance;
           // boolean stopSearch;
            ArrayList<Integer> pairsFirst = new ArrayList<>(), pairsSecond = new ArrayList<>();
            ArrayList<Double> pairDistanceValues = new ArrayList<>();
            ArrayList<Double> distanceValues = new ArrayList<>();
            double minDVal;
            int minPairP1Index = 0, minPairP2Index = 0, deletedIndex;
            int currPVal;
            for (int i = 0; i < points.size(); i++) {
                distanceValues.add(0.0);
            }
            for (int i = 0; i < points.size() - 1; i++) {
                for (int j = i + 1; j < points.size(); j++) {
                    fixedX = false;
                    try {
                        slope = ((double) points.get(j)[1] - points.get(i)[1]) / ((double) points.get(j)[0] - points.get(i)[0]);
                    } catch (ArithmeticException e) {
                        fixedX = true;
                    }
                    intercept = (double) points.get(i)[1] - slope * points.get(i)[0];
                    //topDiv = Integer.MIN_VALUE;
                    topDiv = diffImg.getPixel(points.get(i)[0], points.get(i)[1])[0] > diffImg.getPixel(points.get(j)[0], points.get(j)[1])[0] ? diffImg.getPixel(points.get(i)[0], points.get(i)[1])[0] : diffImg.getPixel(points.get(j)[0], points.get(j)[1])[0];
                    bottomDiv = Integer.MAX_VALUE;
                    for (k = points.get(i)[1], kMax = points.get(j)[1], direction = ((points.get(i)[1] < points.get(j)[1]) ? 1 : (-1)); k != kMax; k += direction) {
                        v = (int) (fixedX ? points.get(i)[0] : ((k - intercept) / slope));
                        currPVal = diffImg.getPixel(v, k)[0];
                        //if (currPVal>topDiv) {
                        //    topDiv = currPVal;
                        //}
                        if (currPVal<bottomDiv) {
                            bottomDiv = currPVal;
                        }
                    }
                    for (k = points.get(j)[0], kMax = points.get(i)[0], direction = ((points.get(j)[0] < points.get(i)[0]) ? 1 : (-1)); k != kMax; k += direction) {
                        v = (int) (intercept + slope * k);
                        currPVal = diffImg.getPixel(k,v)[0];
                        //if (currPVal>topDiv) {
                        //    topDiv = currPVal;
                        //}
                        if (currPVal<bottomDiv) {
                            bottomDiv = currPVal;
                        }
                    }
                    pairsFirst.add(i);
                    pairsSecond.add(j);
                    currDistance = topDiv-bottomDiv;
                    pairDistanceValues.add(currDistance);
                    distanceValues.set(i, distanceValues.get(i) + currDistance);
                    distanceValues.set(j, distanceValues.get(j) + currDistance);
                }
            }
            minDVal = Double.MAX_VALUE;
            for (int i = 0; i < pairDistanceValues.size(); i++) {
                if (minDVal > pairDistanceValues.get(i)) {
                    minDVal = pairDistanceValues.get(i);
                    minPairP1Index = pairsFirst.get(i);
                    minPairP2Index = pairsSecond.get(i);
                }
            }
            if (distanceValues.get(minPairP1Index) > distanceValues.get(minPairP2Index)) {
                deletedIndex = minPairP2Index;
                //points.get(minPairP1Index)[0] = (int) (points.get(minPairP1Index)[0]*(0.4)+points.get(minPairP2Index)[0]*(0.6));
                //points.get(minPairP1Index)[1] = (int) (points.get(minPairP1Index)[1]*(0.4)+points.get(minPairP2Index)[1]*(0.6));
            } else {
                deletedIndex = minPairP1Index;
                //points.get(minPairP2Index)[0] = (int) (points.get(minPairP2Index)[0]*(0.4)+points.get(minPairP1Index)[0]*(0.6));
                //points.get(minPairP2Index)[1] = (int) (points.get(minPairP2Index)[1]*(0.4)+points.get(minPairP1Index)[1]*(0.6));
            }
            points.remove(deletedIndex);
            distanceValues.remove(deletedIndex);
        }
        return points;
    }

    public static ArrayList<Integer[]> decodeCsvPoints(String filename) throws IOException {
        ArrayList<Integer[]> points = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line = null;
        int i = 0;
        String[] pointData;
        Integer[] pointDataInt = new Integer[2];
        while ((line = br.readLine()) != null) {
            if (i != 0) {
                pointData = line.split(",");
                //System.out.println(line+" - "+pointData.length);
                pointDataInt[0] = Integer.parseInt(pointData[1]);
                pointDataInt[1] = Integer.parseInt(pointData[2]);
                points.add(pointDataInt.clone());
            }
            i++;
        }
        br.close();
        return points;
    }
    
    private static void markPoints (ImagePlus img, ArrayList<Integer[]> points, int markRadius) {
        IJ.setForegroundColor(9, 255, 0);
        for (int i=0; i<points.size(); i++) {
            img.setRoi(new OvalRoi(points.get(i)[0], points.get(i)[1],markRadius,markRadius));
            IJ.run(img, "Fill", "slice");
        }
    }
    
    private static void movePointsToLocalMaxima(ArrayList<Integer[]> points, ImagePlus img, int searchRadius) {
        int searchRegionX1, searchRegionY1, searchRegionX2, searchRegionY2;
        final int imgWidth = img.getWidth(), imgHeight = img.getHeight();
        int maxValue, maxValueX, maxValueY, tmpValue;
        for (Integer[] point : points) {
            searchRegionX1 = point[0]-searchRadius/2;
            searchRegionX1 = searchRegionX1>=0 ? searchRegionX1 : 0;
            searchRegionX2 = point[0]+searchRadius/2;
            searchRegionX2 = searchRegionX2<imgWidth ? searchRegionX2 : 0;
            searchRegionY1 = point[1]-searchRadius/2;
            searchRegionY1 = searchRegionY1>=0 ? searchRegionY1 : 0;
            searchRegionY2 = point[1]+searchRadius/2;
            searchRegionY2 = searchRegionY2<imgHeight ? searchRegionY2 : 0;
            maxValue = img.getPixel(point[0],point[1])[0];
            maxValueX = point[0];
            maxValueY = point[1];
            for (int i=searchRegionX1; i<=searchRegionX2; i++) {
                for (int j=searchRegionY1; j<=searchRegionY2; j++) {
                    tmpValue = img.getPixel(i,j)[0];
                    if (tmpValue>maxValue) {
                        maxValue = tmpValue;
                        maxValueX = i;
                        maxValueY = j;
                    }
                }
            }
            point[0] = maxValueX;
            point[1] = maxValueY;
        }
    }

    private static ArrayList<Integer[]> findPointsImageJ(String path, String imgFile1, String imgFile2, int pCount) throws IOException {
        ArrayList<HashMap<String, Integer>> paramVariants = new ArrayList<>();
        HashMap<String, Integer> params = new HashMap<>();
        params.put("Mean", 10);
        params.put("Subtract", 25);
        params.put("Maximum", 10);
        params.put("Erode", 10);
        params.put("Dilate", 5);
        paramVariants.add((HashMap<String, Integer>) params.clone());
        params.clear();
        params.put("Mean", 0);
        params.put("Subtract", 50);
        params.put("Maximum", 10);
        params.put("Erode", 5);
        params.put("Dilate", 10);
        paramVariants.add((HashMap<String, Integer>) params.clone());
        params.clear();
        params.put("Mean", 0);
        params.put("Subtract", 20);
        params.put("Maximum", 10);
        params.put("Erode", 5);
        params.put("Dilate", 10);
        paramVariants.add((HashMap<String, Integer>) params.clone());
        params.clear();
        params.put("Mean", 0);
        params.put("Subtract", 75);
        params.put("Maximum", 0);
        params.put("Erode", 0);
        params.put("Dilate", 10);
        paramVariants.add((HashMap<String, Integer>) params.clone());
        params.clear();
        params.put("Mean", 0);
        params.put("Subtract", 75);
        params.put("Maximum", 0);
        params.put("Erode", 0);
        params.put("Dilate", 3);
        paramVariants.add((HashMap<String, Integer>) params.clone());
        ArrayList<Integer[]> maxFoundPoints = new ArrayList<Integer[]>();
        for (HashMap<String, Integer> paramVariant : paramVariants) {
            String fileNameBase = imgFile2.substring(0, imgFile2.lastIndexOf('.'));
            Opener opener = new Opener();
            ImageCalculator ic = new ImageCalculator();
            ImagePlus imp1;
            ImagePlus imp2;
            ImagePlus imp3;
            Prefs.blackBackground = true;
            imp1 = opener.openImage(path + imgFile1);
            imp2 = opener.openImage(path + imgFile2);
            imp2 = ic.run("Difference create", imp1, imp2);
            ImagePlus[] channels1 = ChannelSplitter.split(imp2);
            ImagePlus[] channels2 = ChannelSplitter.split(imp2);
            imp1 = ic.run("Max create", channels1[0], channels1[1]);
            imp1 = ic.run("Max create", imp1, channels1[2]);
            imp2 = ic.run("Min create", channels2[0], channels2[1]);
            imp2 = ic.run("Min create", imp2, channels2[2]);
            imp2 = ic.run("Add create", imp1, imp2);
            imp3 = imp2.duplicate();
            IJ.run(imp3, "Mean...", "radius=5");
            Prefs.blackBackground = true;
            if (printDebugInfo) {
                IJ.saveAs(imp3, "PNG", path + fileNameBase + "_preop.png");
            }
            if (paramVariant.containsKey("Mean") && paramVariant.get("Mean")>0) {
                IJ.run(imp2, "Mean...", "radius="+paramVariant.get("Mean"));
            }
            if (paramVariant.containsKey("Subtract") && paramVariant.get("Subtract")>0) {
                IJ.run(imp2, "Subtract...", "value="+paramVariant.get("Subtract"));
            }
            if (paramVariant.containsKey("Maximum") && paramVariant.get("Maximum")>0) {
                IJ.run(imp2, "Maximum...", "radius="+paramVariant.get("Maximum"));
            }
            //IJ.saveAs(imp2, "PNG", path + fileNameBase + "_mx.png");
            if (printDebugInfo) {
                IJ.saveAs(imp2, "PNG", path + fileNameBase + "_mx.png");
            }
            //IJ.getImage().close(); - вырубать картиночки
            IJ.run(imp2, "Square", "");
            if (printDebugInfo) {
                IJ.saveAs(imp2, "PNG", path + fileNameBase + "_diffs.png");
            }
            //IJ.saveAs(imp2, "PNG", path + fileNameBase + "_mx.png");
            IJ.run(imp2, "Make Binary", "");
            if (paramVariant.containsKey("Erode") && paramVariant.get("Erode")>0) {
                IJ.run(imp2, "Options...", "iterations="+paramVariant.get("Erode")+" count=1 black do=Erode");
            }
            if (paramVariant.containsKey("Dilate") && paramVariant.get("Dilate")>0) {
                IJ.run(imp2, "Options...", "iterations="+paramVariant.get("Dilate")+" count=1 black do=Dilate");
            }
            if (printDebugInfo) {
                IJ.saveAs(imp2, "PNG", path + fileNameBase + "_regions.png");
            }
            IJ.run(imp2, "Find Maxima...", "noise=10 output=List");
            IJ.saveAs("Results", path + fileNameBase + "_results.csv");
            //int[] ( PointRoi )ImagePlus.getRoi().getXCoordinates();
            //int[] ( PointRoi )ImagePlus.getRoi().getYCoordinates(); 
            ArrayList<Integer[]> points = new ArrayList<>();
            try {
                points = decodeCsvPoints(path + fileNameBase + "_results.csv");
            }
            catch (Exception e) {
                IJ.getErrorMessage();
                throw e;
            }
            if (!printDebugInfo) {
                new File(path + fileNameBase + "_results.csv").delete();
            }
            else {
                System.out.print(pCount+" <= "+points.size());
            }
             if (printDebugInfo) {
                IJ.run(imp1, "RGB Color", "");
                markPoints(imp1,points,4);
                //IJ.saveAs(imp1, "PNG", path + fileNameBase + "_prePoints.png");
            }
            deletePointsByDistance(points,imp2,pCount);
            movePointsToLocalMaxima(points, imp3, 28);
            //deletePointsByDiversity(points, imp3, pCount);
            //Можно, если по дивёрсити удалять, джойнить с ближайшими
            if (printDebugInfo) {
                System.out.print("; "+pCount+" = "+points.size()+"\n");
                IJ.run(imp1, "RGB Color", "");
                markPoints(imp1,points,10);
                //markPoints(imp1,points,1);
                IJ.saveAs(imp1, "PNG", path + fileNameBase + "_finalPoints.png");
            }
            if (points.size()==pCount) {
                return points;
            }
            else if (points.size()>maxFoundPoints.size()) {
                maxFoundPoints = points;
            }
        }
        return maxFoundPoints;
    }

    public static ArrayList<Integer[]> findDiffPoints(String path, String imgFile1, String imgFile2, int pCount) throws IOException {
        ArrayList<Integer[]> points = findPointsImageJ(path, imgFile1, imgFile2, pCount);

        return points;
    }
}
