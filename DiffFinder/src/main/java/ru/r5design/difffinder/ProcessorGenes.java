/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.r5design.difffinder;

import java.util.HashMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.Gene;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.BooleanGene;
import org.jgap.impl.DoubleGene;
import org.jgap.impl.IntegerGene;

enum ProcessorGenes
{
    INSTANCE;

    // instance vars, constructor
    private final DualHashBidiMap<String, Integer> paramsMapping;
    private final DualHashBidiMap<String, Object[]> paramsBounds;
    public final int paramsCount;
    
    //TODO: Потом выпилить.
    /*
    public final HashMap<String, Object> sampleParams;
    private void setSampleParams(HashMap<String, Object> sampleParams) {
        sampleParams.put("red_join_green", 9);
        sampleParams.put("rg_join_blue", 9);
        sampleParams.put("mean", 5.0);
        sampleParams.put("subtract", 25);
        sampleParams.put("maximum", 15);
        sampleParams.put("multiply", 2.5);
        sampleParams.put("erode_iters", 10);
        sampleParams.put("erode_count", 3);
        sampleParams.put("dilate_iters", 5);
        sampleParams.put("dilate_count", 1);
    }
*/
    
    public Chromosome getChromosomeFromStringParams(Configuration conf, HashMap<String, String> stringParams) throws InvalidConfigurationException {
        HashMap <String, Object> params = new HashMap<>();
        for (int i=0; i<this.paramsCount; i++) {
            String geneName = this.getGeneName(i);
            Object eObj = this.getGeneBounds(geneName)[0];
            if (eObj.getClass() == Boolean.class) {
                params.put(geneName, Boolean.parseBoolean(stringParams.get(geneName)));
            }
            else if (eObj.getClass() == Integer.class) {
                params.put(geneName, Integer.parseInt(stringParams.get(geneName)));
            }
            else if (eObj.getClass() == Double.class) {
                params.put(geneName, Double.parseDouble(stringParams.get(geneName)));
            }
        }
        return getChromosomeFromParams(conf,params);
    }
    
    public Chromosome getChromosomeFromParams(Configuration conf, HashMap<String, Object> params) throws InvalidConfigurationException {
        Chromosome c = getExampleChromosome(conf);
        for (int i=0; i<this.paramsCount; i++) {
            c.getGene(i).setAllele(params.get(this.getGeneName(i)));
        }
        return c;
    }
    
    public Chromosome getExampleChromosome (Configuration conf) throws InvalidConfigurationException {
        Gene[] genes = new Gene[this.paramsCount];
        for (int i=0; i<this.paramsCount; i++) {
                String geneName = this.getGeneName(i);
                Object[] bounds = this.getGeneBounds(geneName);
                if (bounds[0].getClass()==Boolean.class) {
                    genes[i] = new BooleanGene(conf);
                }
                else if(bounds[0].getClass()==Integer.class) {
                    genes[i] = new IntegerGene(conf, (Integer)bounds[0], (Integer)bounds[1]);
                }
                else if(bounds[0].getClass()==Double.class) {
                    genes[i] = new DoubleGene(conf, (Double)bounds[0], (Double)bounds[1]);
                }
            }
        Chromosome pS = new Chromosome(conf, genes);
        return pS;
    }
    
    public static double roundDouble(double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }
    
    //Дефолтное округление значений дабловых генов
    public static double roundDoubleDefault(double number) {
        return roundDouble(number,2);
    }
    /*
    public void setExampleChromosome(IChromosome ch) {
        for (int i=0; i<paramsCount; i++) {
            ch.getGene(i).setAllele(sampleParams.get(getGeneName(i)));
        }
    }
*/

    public static String getImageCalculationName (int id) {
        switch (id) {
            case 2:
                return "Add";
            case 3:
                return "Multiply";
            case 4:
                return "AND";
            case 5:
                return "OR";
            case 6:
                return "XOR";
            case 7:
                return "Average";
            case 8:
                return "Difference";
            case 9:
                return "Max";
            case 10:
                return "Min";
            default:
                return "";
        }
    }
    
    //TODO: Find Edges, или аналог
    ProcessorGenes()
    {
        int i=0;
        paramsMapping = new DualHashBidiMap<>();
        paramsBounds = new DualHashBidiMap<>();
        //Контрастность (0-100%)
        paramsMapping.put("contrast", i++);
        paramsBounds.put("contrast", (Object[])(new Double[]{20.0,75.0}));
        //0 - select first, 1 - select second, 2 - add, 3 - multiply, 4 - and, 5 - or, 6 - xor, 7 - average, 8 - difference
        //Домножалки каналов? Было б неплохо.
        paramsMapping.put("red_join_green", i++);
        paramsBounds.put("red_join_green", (Object[])(new Integer[]{0,10}));
        paramsMapping.put("rg_join_blue", i++);
        paramsBounds.put("rg_join_blue", (Object[])(new Integer[]{0,10}));
        //Фильтр Mean
        paramsMapping.put("mean", i++);
        paramsBounds.put("mean", (Object[])(new Double[]{0.0,50.0}));
        //Фильтр Subtract
        paramsMapping.put("subtract", i++);
        paramsBounds.put("subtract", (Object[])(new Integer[]{0,255}));
        //Фильтр Maximum
        paramsMapping.put("maximum", i++);
        paramsBounds.put("maximum", (Object[])(new Integer[]{0,100}));
        //Фильтр Multiply
        paramsMapping.put("multiply", i++);
        paramsBounds.put("multiply", (Object[])(new Double[]{0.0,10.0}));
        //Binary делаем в любом случае
        //Фильтр Erode
        //Итерации
        paramsMapping.put("erode_iters", i++);
        paramsBounds.put("erode_iters", (Object[])(new Integer[]{0,100}));
        //Каунт (хз)
        paramsMapping.put("erode_count", i++);
        paramsBounds.put("erode_count", (Object[])(new Integer[]{0,8}));
        //Фильтр Dilate
        //Итерации
        paramsMapping.put("dilate_iters", i++);
        paramsBounds.put("dilate_iters", (Object[])(new Integer[]{0,100}));
        //Каунт
        paramsMapping.put("dilate_count", i++);
        paramsBounds.put("dilate_count", (Object[])(new Integer[]{0,8}));
        //TODO: Второй набор генов - без бинари (там тоже максимум ищется)
        //Отсеивание шума при поиске точек максимума (Пока не нужно - Binary)
        //paramsMapping.put("maxima_noize", i++);
        paramsCount = i;
        //sampleParams = new HashMap<>();
        //setSampleParams(sampleParams);
    }

    // Static getter
    public static ProcessorGenes getInstance()
    {
        return INSTANCE;
    }

    public int getGeneId(String name) {
        return paramsMapping.get(name);
    }
    
    public String getGeneName(int id) {
        return paramsMapping.getKey(id);
    }
    
    public Object[] getGeneBounds(String name) {
        return paramsBounds.get(name);
    }
}