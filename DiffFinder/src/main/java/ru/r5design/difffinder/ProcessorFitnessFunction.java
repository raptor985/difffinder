/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.r5design.difffinder;

import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.gui.OvalRoi;
import ij.io.Opener;
import ij.plugin.ChannelSplitter;
import ij.plugin.ImageCalculator;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.FitnessFunction;
import org.jgap.Gene;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author R5
 */
public class ProcessorFitnessFunction extends FitnessFunction {

    final ProcessorGenes genesMap = ProcessorGenes.getInstance();
    final float eps = (float) 0.5;
    //final ArrayList<String[]> imgNames;
    //final HashMap<String, BufferedImage> checkImages;
    //final ArrayList<ArrayList<Rectangle>> checkRects;
    final String basePath;
    final String outPath;
    final DBOperator dbOp = DBOperator.getInstance();
    final TestImages tI = TestImages.getInstance();

    //Костыльненько
    private static final int MAX_FITNESS_STORE_SIZE = 4096;
    private final Map<String, Double> geneFitness;

    /*
    public void loadTestImagesData(ArrayList<String[]> imgNames, ArrayList<ArrayList<Rectangle>> rects) throws IOException, ParseException, SQLException, ClassNotFoundException {
        ArrayList<String> fileBaseNames = new ArrayList<>();
        HashMap<String, ArrayList<Rectangle>> imgsRects = new HashMap<>();
        dbOp.getImgsRects(fileBaseNames, imgsRects);
        BufferedImage a, b;
        for (int i = 0; i < fileBaseNames.size(); i++) {
            a = ImageIO.read(new File(basePath + fileBaseNames.get(i) + "_1.png"));
            b = ImageIO.read(new File(basePath + fileBaseNames.get(i) + "_2.png"));
            //checkImages.put(fileBaseNames.get(i) + "_1.png", a);
            //checkImages.put(fileBaseNames.get(i) + "_2.png", b);
            imgNames.add(new String[]{(fileBaseNames.get(i) + "_1.png"), (fileBaseNames.get(i) + "_2.png")});
            //checkImages.put(fileBaseNames.get(i) + "_2.png", b);
            rects.add(imgsRects.get(fileBaseNames.get(i)));
        }
    }
     */
    public void serializeGenes(String filename, IChromosome[] ics) throws IOException {
        JSONArray scs = new JSONArray();
        JSONObject sc;
        for (int i = 0; i < ics.length; i++) {
            sc = new JSONObject();
            for (int j = 0; j < genesMap.paramsCount; j++) {
                sc.put(genesMap.getGeneName(j), ics[i].getGene(j).getAllele());
            }
            scs.add(sc);
        }
        FileWriter fw = new FileWriter(new File(filename));
        scs.writeJSONString(fw);
        fw.close();
    }

    public Chromosome deserializeGenes(String path, Configuration conf) throws InvalidConfigurationException, FileNotFoundException, IOException, ParseException {
        HashMap<String, String> params = new HashMap<>();
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(path));
        JSONObject jObj = (JSONObject) ((JSONArray) obj).get(0);
        ProcessorGenes pG = ProcessorGenes.getInstance();
        for (int i = 0; i < pG.paramsCount; i++) {
            params.put(pG.getGeneName(i), jObj.get(pG.getGeneName(i)).toString());
        }
        //Chromosome pS=genesMap.getExampleChromosome(conf);
        return pG.getChromosomeFromStringParams(conf, params);
    }

    public void serializeGenes(String filename, IChromosome ics) throws IOException {
        serializeGenes(filename, new IChromosome[]{ics});
    }

    public static ArrayList<Point> decodeCsvPoints(String filename) {
        ArrayList<Point> points = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line = null;
            int i = 0;
            String[] pointDataStr;
            Point pointData;
            while ((line = br.readLine()) != null) {
                if (i != 0) {
                    pointDataStr = line.split(",");
                    pointData = new Point(Integer.parseInt(pointDataStr[1]), Integer.parseInt(pointDataStr[2]));
                    points.add(pointData);
                }
                i++;
            }
            br.close();
        } catch (IOException e) {
        }
        return points;
    }

    //public ArrayList<Point> getImagePoints (String imgFile1, String imgFile2, IChromosome pS) throws IOException {
    public ArrayList<Point> getImagePoints(BufferedImage image1, BufferedImage image2, IChromosome pS) {
        int tValInt, tValInt2;
        double tValDbl;
        ImageCalculator ic = new ImageCalculator();
        ImagePlus imp1 = new ImagePlus("first", image1);
        ImagePlus imp2 = new ImagePlus("first", image2);
        tValDbl = (double) pS.getGene(genesMap.getGeneId("contrast")).getAllele();
        if (tValDbl >= eps) {
            tValDbl = genesMap.roundDoubleDefault(tValDbl);
            IJ.run(imp1, "Enhance Contrast...", "saturated=" + tValDbl);
            IJ.run(imp2, "Enhance Contrast...", "saturated=" + tValDbl);
        }
        ImagePlus imp3 = ic.run("Difference create", imp1, imp2);
        Prefs.blackBackground = true;
        ImagePlus[] channels = ChannelSplitter.split(imp3);
        ImagePlus irg, irgb;
        tValInt = (int) pS.getGene(genesMap.getGeneId("red_join_green")).getAllele();
        if (tValInt > 1) {
            irg = ic.run(ProcessorGenes.getImageCalculationName(tValInt) + " create", channels[0], channels[1]);
        } else if (tValInt == 1) {
            irg = channels[1];
        } else {
            irg = channels[0];
        }
        tValInt = (int) pS.getGene(genesMap.getGeneId("rg_join_blue")).getAllele();
        if (tValInt > 1) {
            irgb = ic.run(ProcessorGenes.getImageCalculationName(tValInt) + " create", irg, channels[2]);
        } else if (tValInt == 1) {
            irgb = channels[2];
        } else {
            irgb = irg;
        }
        tValDbl = (double) pS.getGene(genesMap.getGeneId("mean")).getAllele();
        if (tValDbl >= eps) {
            tValDbl = genesMap.roundDoubleDefault(tValDbl);
            IJ.run(irgb, "Mean...", "radius=" + tValDbl);
        }
        tValInt = (int) pS.getGene(genesMap.getGeneId("subtract")).getAllele();
        if (tValInt > 0) {
            IJ.run(irgb, "Subtract...", "value=" + tValInt);
        }
        tValInt = (int) pS.getGene(genesMap.getGeneId("maximum")).getAllele();
        if (tValInt > 0) {
            IJ.run(irgb, "Maximum...", "radius=" + tValInt);
        }
        tValDbl = (double) pS.getGene(genesMap.getGeneId("multiply")).getAllele();
        if (tValDbl >= eps) {
            tValDbl = genesMap.roundDoubleDefault(tValDbl);
            IJ.run(irgb, "Multiply...", "value=" + tValDbl);
        }
        IJ.run(irgb, "Make Binary", "");
        tValInt = (int) pS.getGene(genesMap.getGeneId("erode_iters")).getAllele();
        tValInt2 = (int) pS.getGene(genesMap.getGeneId("erode_count")).getAllele();
        //8 почти эффекта не даст, а на итерациях отожрёт
        if (tValInt > 0 && tValInt2 < 8) {
            IJ.run(irgb, "Options...", "iterations=" + tValInt + " count=" + tValInt2 + " black do=Erode");
        }
        tValInt = (int) pS.getGene(genesMap.getGeneId("dilate_iters")).getAllele();
        tValInt2 = (int) pS.getGene(genesMap.getGeneId("dilate_count")).getAllele();
        if (tValInt > 0 && tValInt2 < 8) {
            IJ.run(irgb, "Options...", "iterations=" + tValInt + " count=" + tValInt2 + " black do=Dilate");
        }
        IJ.run(irgb, "Find Maxima...", "noise=0 output=List");
        String tmpFileName = "" + (new Date()).getTime() + ".csv";
        IJ.saveAs("Results", outPath + tmpFileName);
        ArrayList<Point> points = decodeCsvPoints(outPath + tmpFileName);
        new File(outPath + tmpFileName).delete();
        return points;
    }

    private static void markPoints(ImagePlus img, ArrayList<Point> points, int markRadius) {
        IJ.setForegroundColor(9, 255, 0);
        for (int i = 0; i < points.size(); i++) {
            img.setRoi(new OvalRoi(points.get(i).x, points.get(i).y, markRadius, markRadius));
            IJ.run(img, "Fill", "slice");
        }
    }

    public void markImagePoints(String outputBasePath, String imgName, BufferedImage image1, BufferedImage image2, IChromosome pS) throws IOException {
        //Opener opener = new Opener();
        ImageCalculator ic = new ImageCalculator();
        ImagePlus imp1 = new ImagePlus("first", image1);
        ImagePlus imp2 = new ImagePlus("second", image2);
        ImagePlus imp3 = ic.run("Difference create", imp1, imp2);
        markPoints(imp3, getImagePoints(image1, image2, pS), 5);
        String oPath;
        if (outputBasePath != null) {
            oPath = outputBasePath;
        } else {
            oPath = outPath;
        }
        IJ.saveAs(imp3, "PNG", oPath + imgName + "_points.png");
    }

    public ProcessorFitnessFunction(String imgBasePath, String outputPath) throws IOException, ParseException, SQLException, ClassNotFoundException {
        //imgNames = new ArrayList<>();
        //checkRects = new ArrayList<>();
        basePath = imgBasePath;
        outPath = outputPath;
        geneFitness = new HashMap<>();
        //checkImages = new HashMap<>();
        //loadTestImagesData(imgNames, checkRects);
    }

    private String hashGenes(Gene[] genes) {
        StringBuilder geneString = new StringBuilder();
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ProcessorFitnessFunction.class.getName()).log(Level.SEVERE, null, ex);
        }
        geneString.append('$');
        geneString.append(tI.activeImages.size());
        geneString.append('$');
        geneString.append(tI.activeImages.get(tI.activeImages.size()-1));
        for (int i = 0; i < genes.length; i++) {
            geneString.append('#');
            geneString.append(i);
            geneString.append('#');
            geneString.append(genes[i].getAllele().getClass() != Double.class ? genes[i].getAllele() : genesMap.roundDoubleDefault((Double) genes[i].getAllele()));
        }
        return new String(String.valueOf(geneString).getBytes());
    }

    /*
    public void getFailImages(IChromosome ic) {
        //Ну вот хз чем инициализировать
        double score = 0;//=imgNames.size()*100;
        //Дофига рискованно, хэши могут совпасть
        String gH = hashGenes(ic.getGenes());
        if (!geneFitness.containsKey(gH)) {
            ArrayList<boolean[]> checkRectsChecked = new ArrayList<>();
            //= (ArrayList<ArrayList<Rectangle>>) checkRects.clone();
            for (int i = 0; i < checkRects.size(); i++) {
                checkRectsChecked.add(new boolean[checkRects.get(i).size()]);
                for (int j = 0; j < checkRects.get(i).size(); j++) {
                    checkRectsChecked.get(i)[j] = false;
                }
            }
            ArrayList<Point> points;
            try {
                final double sameRectsStartPenalty = 0.1, sameRectsPenaltyMultiplier = 2;
                final double noPointRectsStartPenalty = 1, noPointRectsPenaltyMultiplier = 3;
                final double noRectPointsStartPenalty = 1, noRectPointsPenaltyMultiplier = 2;
                for (int i = 0; i < imgNames.size(); i++) {
                    //points = getImagePoints(imgNames.get(i)[0], imgNames.get(i)[1],ic);
                    points = getImagePoints(checkImages.get(imgNames.get(i)[0]), checkImages.get(imgNames.get(i)[1]), ic);
                    double sameRectsPenalty = sameRectsStartPenalty;
                    double noPointRectsPenalty = noPointRectsStartPenalty;
                    double noRectPointsPenalty = noRectPointsStartPenalty;
                    for (int j = 0; j < points.size(); j++) {
                        boolean pointRectFound = false;
                        for (int k = 0; k < checkRects.get(i).size(); k++) {
                            //System.out.println(j+" "+k+" : "+points.size()+" : "+checkRects.get(i).size());
                            if (checkRects.get(i).get(k).contains(points.get(j))) {
                                if (!checkRectsChecked.get(i)[k]) {
                                    checkRectsChecked.get(i)[k] = true;
                                    score += 1.0;
                                } else {
                                    score -= sameRectsPenalty;
                                    sameRectsPenalty *= sameRectsPenaltyMultiplier;
                                }
                                pointRectFound = true;
                            }
                        }
                        if (pointRectFound) {
                            points.remove(j);
                            j--;
                        }
                    }
                    boolean uncheckedRect = false;
                    for (int j = 0; j < checkRects.get(i).size(); j++) {
                        if (!checkRectsChecked.get(i)[j]) {
                            score -= noRectPointsPenalty;
                            noRectPointsPenalty *= noRectPointsPenaltyMultiplier;
                            uncheckedRect = true;
                        }
                    }
                    if (uncheckedRect) {
                        markImagePoints(outPath, imgNames.get(i)[0], imgNames.get(i)[1], ic);
                    }
                    for (int j = 0; j < points.size(); j++) {
                        score -= noPointRectsPenalty;
                        noPointRectsPenalty *= noPointRectsPenaltyMultiplier;
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(ProcessorFitnessFunction.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.print("calcfit ");
            Gene[] genes = ic.getGenes();
            for (int i = 0; i < genes.length; i++) {
                System.out.print(genes[i].getAllele() + " ");
            }
            System.out.print(" = " + score + "\n");
            if (geneFitness.size() >= MAX_FITNESS_STORE_SIZE) {
                geneFitness.clear();
            }
            geneFitness.put(gH, score);
        } else {
            score = geneFitness.get(gH);
        }
    }
     */
    @Override
    public double evaluate(IChromosome ic) {
        /*
        final double sameRects[] = {0.025, 1.0, 0.5};
        final double noPointRects[] = {3, 2.0, 6.0};
        final double noRectPoints[] = {1.5, 3.0, 4.0};
        */
        final double sameRects[] = {0.025, 1.0, 0.5};
        final double noPointRects[] = {1, 1.0, 1.0};
        final double noRectPoints[] = {6, 3.0, 6.0};

        double score = 0;
        String gH = hashGenes(ic.getGenes());
        if (!geneFitness.containsKey(gH)) {
            for (int i = 0; i < tI.activeImages.size(); i++) {
                String imgName = tI.activeImages.get(i);
                ArrayList<Rectangle> checkRects = tI.allImagesRects.get(imgName);
                boolean[] checkRectsChecked = new boolean[checkRects.size()];
                Arrays.fill(checkRectsChecked, false);
                ArrayList<Point> points;
                points = getImagePoints(tI.allImagesFiles.get(imgName)[0], tI.allImagesFiles.get(imgName)[1], ic);
                double sameRectsPenalty = sameRects[0];
                double noPointRectsPenalty = noPointRects[0];
                double noRectPointsPenalty = noRectPoints[0];
                for (int j = 0; j < points.size(); j++) {
                    boolean pointRectFound = false;
                    for (int k = 0; k < checkRects.size(); k++) {
                        if (checkRects.get(k).contains(points.get(j))) {
                            if (!checkRectsChecked[k]) {
                                checkRectsChecked[k] = true;
                                score += 1.0;
                            } else {
                                score -= sameRectsPenalty;
                                sameRectsPenalty *= sameRects[1];
                                sameRectsPenalty = (sameRectsPenalty > sameRects[2]) ? sameRects[2] : sameRectsPenalty;
                            }
                            pointRectFound = true;
                        }
                    }
                    if (pointRectFound) {
                        points.remove(j);
                        j--;
                    }
                }
                for (int j = 0; j < checkRects.size(); j++) {
                    if (!checkRectsChecked[j]) {
                        score -= noRectPointsPenalty;
                        noRectPointsPenalty *= noRectPoints[1];
                        noRectPointsPenalty = (noRectPointsPenalty > noRectPoints[2]) ? noRectPoints[2] : noRectPointsPenalty;
                    }
                }
                for (int j = 0; j < points.size(); j++) {
                    score -= noPointRectsPenalty;
                    noPointRectsPenalty *= noPointRects[1];
                    noPointRectsPenalty = (noPointRectsPenalty > noPointRects[2]) ? noPointRects[2] : noPointRectsPenalty;
                }
            }
            System.out.print("calcfit ");
            Gene[] genes = ic.getGenes();
            for (int i = 0; i < genes.length; i++) {
                System.out.print(genes[i].getAllele() + " ");
            }
            System.out.print(" = " + score + "\n");
            if (geneFitness.size() >= MAX_FITNESS_STORE_SIZE) {
                geneFitness.clear();
            }
            geneFitness.put(gH, score);
        } else {
            score = geneFitness.get(gH);
        }
        return score;
    }

    Random rnd = new Random();

    //Hightly experimental shit.
    /*
    @Override
    public double evaluate(IChromosome ic) {
        final double sameRects[] = {0.025, 1.0, 0.5};
        final double noPointRects[] = {3, 2.0, 10.0};
        final double noRectPoints[] = {1.5, 3.0, 8.0};

        double score = 0;
        //String gH = hashGenes(ic.getGenes());
        //if (!geneFitness.containsKey(gH)) {
        for (int h=0; h<3; h++) {
            double currScore = 0;
            int i = rnd.nextInt(tI.activeImages.size());
            //for (int i = 0; i < tI.activeImages.size(); i++) {
            String imgName = tI.activeImages.get(i);
            ArrayList<Rectangle> checkRects = tI.allImagesRects.get(imgName);
            boolean[] checkRectsChecked = new boolean[checkRects.size()];
            Arrays.fill(checkRectsChecked, false);
            ArrayList<Point> points;
            points = getImagePoints(tI.allImagesFiles.get(imgName)[0], tI.allImagesFiles.get(imgName)[1], ic);
            double sameRectsPenalty = sameRects[0];
            double noPointRectsPenalty = noPointRects[0];
            double noRectPointsPenalty = noRectPoints[0];
            for (int j = 0; j < points.size(); j++) {
                boolean pointRectFound = false;
                for (int k = 0; k < checkRects.size(); k++) {
                    if (checkRects.get(k).contains(points.get(j))) {
                        if (!checkRectsChecked[k]) {
                            checkRectsChecked[k] = true;
                            currScore += 1.0;
                        } else {
                            currScore -= sameRectsPenalty;
                            sameRectsPenalty *= sameRects[1];
                            sameRectsPenalty = (sameRectsPenalty > sameRects[2]) ? sameRects[2] : sameRectsPenalty;
                        }
                        pointRectFound = true;
                    }
                }
                if (pointRectFound) {
                    points.remove(j);
                    j--;
                }
            }
            for (int j = 0; j < checkRects.size(); j++) {
                if (!checkRectsChecked[j]) {
                    currScore -= noRectPointsPenalty;
                    noRectPointsPenalty *= noRectPoints[1];
                    noRectPointsPenalty = (noRectPointsPenalty > noRectPoints[2]) ? noRectPoints[2] : noRectPointsPenalty;
                }
            }
            for (int j = 0; j < points.size(); j++) {
                currScore -= noPointRectsPenalty;
                noPointRectsPenalty *= noPointRects[1];
                noPointRectsPenalty = (noPointRectsPenalty > noPointRects[2]) ? noPointRects[2] : noPointRectsPenalty;
            }
            currScore /= checkRects.size();
            score += currScore;
        }
        
        //}
        System.out.print("calcfit ");
        Gene[] genes = ic.getGenes();
        for (int i = 0; i < genes.length; i++) {
            System.out.print(genes[i].getAllele() + " ");
        }
        System.out.print(" = " + score + "\n");
        //if (geneFitness.size() >= MAX_FITNESS_STORE_SIZE) {
        //    geneFitness.clear();
        //}
        //geneFitness.put(gH, score);
        //} else {
        //    score = geneFitness.get(gH);
        //}
        return score;
    }
*/

    void flushFitnessCache() {
        geneFitness.clear();
    }

    //Tests one image rects coverage
    public boolean testImage(IChromosome ic, String imgName) {
        ArrayList<Rectangle> checkRects = tI.allImagesRects.get(imgName);
        boolean[] checkRectsChecked = new boolean[checkRects.size()];
        Arrays.fill(checkRectsChecked, false);
        ArrayList<Point> points;
        points = getImagePoints(tI.allImagesFiles.get(imgName)[0], tI.allImagesFiles.get(imgName)[1], ic);
        for (int j = 0; j < points.size(); j++) {
            boolean pointRectFound = false;
            for (int k = 0; k < checkRects.size(); k++) {
                if (checkRects.get(k).contains(points.get(j))) {
                    if (!checkRectsChecked[k]) {
                        checkRectsChecked[k] = true;
                    }
                    pointRectFound = true;
                }
            }
            if (pointRectFound) {
                points.remove(j);
                j--;
            }
        }
        if (points.isEmpty()) {
            for (int j = 0; j < checkRects.size(); j++) {
                if (!checkRectsChecked[j]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public final double getFitnessValue(final IChromosome a_subject) {
        //Почему ограничения на >=0? Какой кретин их придумал?
        return evaluate(a_subject);
    }
}
