/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComparerTest;

import java.awt.Rectangle;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;
import ru.r5design.difffinder.Comparer;

/**
 *
 * @author R5
 */
public class ComparerTest {

    private void loadTestImagesData(ArrayList<String[]> paths, ArrayList<ArrayList<Rectangle>> rects) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(
                "D:\\diffFinder\\testing\\pics\\diffs.json"));
        JSONObject jsonObject = (JSONObject) obj;
        JSONArray testImgsList = (JSONArray) jsonObject.get("list"), tmpRectsList;
        ArrayList<Rectangle> tmpRectsListArr = new ArrayList<>();
        int tmpX, tmpY, tmpWidth, tmpHeight;
        for (int i = 0; i < testImgsList.size(); i++) {
            //System.out.println(((JSONObject)testImgsList.get(i)).get("first"));
            //iterator.next();
            paths.add(new String[]{(String) ((JSONObject) testImgsList.get(i)).get("first"), (String) ((JSONObject) testImgsList.get(i)).get("second")});
            tmpRectsList = (JSONArray) ((JSONObject) testImgsList.get(i)).get("diffs");
            tmpRectsListArr.clear();
            for (int j = 0; j < tmpRectsList.size(); j++) {
                tmpWidth = ((Long) ((Long)((JSONObject) tmpRectsList.get(j)).get("x2") - (Long)((JSONObject) tmpRectsList.get(j)).get("x1"))).intValue();
                if (tmpWidth>0) {
                    tmpX = ((Long) ((JSONObject) tmpRectsList.get(j)).get("x1")).intValue();
                } else {
                    tmpX = ((Long) ((JSONObject) tmpRectsList.get(j)).get("x2")).intValue();
                    tmpWidth = -tmpWidth;
                }
                tmpHeight = ((Long) ((Long) ((JSONObject) tmpRectsList.get(j)).get("y2") - (Long) ((JSONObject) tmpRectsList.get(j)).get("y1"))).intValue();
                if (tmpHeight>0) {
                    tmpY = ((Long) ((JSONObject) tmpRectsList.get(j)).get("y1")).intValue();
                } else {
                    tmpY = ((Long) ((JSONObject) tmpRectsList.get(j)).get("y2")).intValue();
                    tmpHeight = -tmpWidth;
                }
                tmpRectsListArr.add(new Rectangle(tmpX, tmpY, tmpWidth, tmpHeight));
            }
            rects.add((ArrayList<Rectangle>) tmpRectsListArr.clone());
        }
    }

    //Пока не нужно
    //@Test
    public void evaluatesExpression() throws IOException, ParseException {
        ArrayList<String[]> paths = new ArrayList<>();
        ArrayList<ArrayList<Rectangle>> rects = new ArrayList<>();
        loadTestImagesData(paths, rects);
        for (int i = 0; i < paths.size(); i++) {
            System.out.print(paths.get(i)[0] + " - " + paths.get(i)[1] + " [");
            for (int j = 0; j < rects.get(i).size(); j++) {
                System.out.print("{{" + rects.get(i).get(j).getMinX() + "," + rects.get(i).get(j).getMinY() + "}, {" + rects.get(i).get(j).getMaxX() + "," + rects.get(i).get(j).getMaxY() + "}}");
                if (j != rects.get(i).size() - 1) {
                    System.out.print(", ");
                }
            }
            System.out.print("]\n");
        }
        Comparer cmp = new Comparer();
        //ArrayList<Integer[]> points = cmp.decodeCsvPoints("D:\\diffpics\\Results.csv");
        // for (int i = 0; i < points.size(); i++) {
        //    System.out.println(points.get(i)[0]+":"+points.get(i)[1]);
        //}
        ArrayList<Integer[]> tmpPoints;
        ArrayList<Rectangle> tmpRects;
        String imgBasePath = new String("D:\\diffpics\\");
        for (int i = 0; i < paths.size(); i++) {
            System.out.println(paths.get(i)[0]);
            tmpPoints = cmp.findDiffPoints(imgBasePath, paths.get(i)[0], paths.get(i)[1], rects.get(i).size());
            tmpRects = (ArrayList<Rectangle>) rects.get(i).clone();
            for (int j = 0; j < tmpPoints.size(); j++) {
                for (int k = 0; k < tmpRects.size(); k++) {
                    //System.out.println(tmpRects.get(k).toString());
                    //System.out.println(tmpPoints.get(j)[0] + ", " + tmpPoints.get(j)[1]);
                    if (tmpRects.get(k).contains(tmpPoints.get(j)[0],tmpPoints.get(j)[1])) {
                        tmpRects.remove(k);
                        //System.out.println("Removed!");
                    }
                }
            }
            try {
                Assert.assertEquals(tmpRects.size(), 0);
            }
            catch (AssertionError e) {
                System.out.print("Test failed.\nRects:\n");
                for (int j=0; j<tmpRects.size(); j++) {
                    System.out.println(tmpRects.get(j).toString());
                }
                System.out.print("Points:\n");
                for (int j=0; j<tmpPoints.size(); j++) {
                    System.out.println("("+tmpPoints.get(j)[0]+", "+tmpPoints.get(j)[1]+")");
                }
                System.out.println("Image: "+paths.get(i)[0]+"\n");
                throw e;
            }
        }
    }
}
