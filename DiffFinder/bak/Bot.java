/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.r5design.difffinder;

import com.jayway.awaitility.Awaitility;
import com.jayway.awaitility.core.ConditionTimeoutException;
import ij.IJ;
import ij.ImagePlus;
import ij.io.Opener;
import java.awt.AWTException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import static java.util.concurrent.TimeUnit.SECONDS;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author R5
 */
public class Bot {

    static WebDriver driver = null;
    //static int bodyOffsetX, bodyOffsetY;
    static int gameWindowOffsetX, gameWindowOffsetY;
    static Robot robot = null;

    private static void moveAndClick(Robot rbt, int x, int y) {
        //rbt.mouseMove(x, y);
        mouseMove(rbt, x, y);
        rbt.delay(200);
        rbt.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        rbt.delay(100);
        rbt.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        rbt.delay(200);
    }

    //tmp
    private static void mouseMove(Robot rbt, int x, int y) {
        rbt.mouseMove(x, y);
        rbt.delay(200);
    }

    private static final boolean isImgExact(BufferedImage img1, BufferedImage img2) {
        int width1 = img1.getWidth(null);
        int width2 = img2.getWidth(null);
        int height1 = img1.getHeight(null);
        int height2 = img2.getHeight(null);
        if ((width1 != width2) || (height1 != height2)) {
            System.err.println("Error: Images dimensions mismatch");
            System.exit(1);
        }
        long diff = 0;
        for (int y = 0; y < height1; y++) {
            for (int x = 0; x < width1; x++) {
                int rgb1 = img1.getRGB(x, y);
                int rgb2 = img2.getRGB(x, y);
                int r1 = (rgb1 >> 16) & 0xff;
                int g1 = (rgb1 >> 8) & 0xff;
                int b1 = (rgb1) & 0xff;
                int r2 = (rgb2 >> 16) & 0xff;
                int g2 = (rgb2 >> 8) & 0xff;
                int b2 = (rgb2) & 0xff;
                diff += Math.abs(r1 - r2);
                diff += Math.abs(g1 - g2);
                diff += Math.abs(b1 - b2);
                if (diff>0) {
                    return false;
                }
            }
        }
        double n = width1 * height1 * 3;
        double p = diff / n / 255.0;
        //System.out.println("diff percent: " + (p * 100.0)+" "+(p<0.0001));
        return (p < 0.0001);
    }

    private static long getPixelDiff(int rgb1, int rgb2) {
        long diff = 0;
        int r1 = (rgb1 >> 16) & 0xff;
        int g1 = (rgb1 >> 8) & 0xff;
        int b1 = (rgb1) & 0xff;
        int r2 = (rgb2 >> 16) & 0xff;
        int g2 = (rgb2 >> 8) & 0xff;
        int b2 = (rgb2) & 0xff;
        diff += Math.abs(r1 - r2);
        diff += Math.abs(g1 - g2);
        diff += Math.abs(b1 - b2);
        return diff;
    }

    private static Rectangle getImgDiffRect(BufferedImage img1, BufferedImage img2) {
        final int ignoreDiffLevel=10;
        final int width = img1.getWidth(), height = img1.getHeight();
        int left = Integer.MAX_VALUE, right = Integer.MIN_VALUE, top = Integer.MAX_VALUE, bottom = Integer.MIN_VALUE;
        for (int i=0; i<height; i++) {
            for (int j=0; j<width; j++) {
                if (getPixelDiff(img1.getRGB(j, i),img2.getRGB(j, i)) > ignoreDiffLevel) {
                    if (left>j) {
                        left=j;
                    }
                    if (right<j) {
                        right=j;
                    }
                    if (top>i) {
                        top=i;
                    }
                    if (bottom<i) {
                        bottom=i;
                    }
                }
            }
        }
        final int bXCenter = (left+right)/2;
        final int bYCenter = (top+bottom)/2;
        //При пересечении прямоугольников могут съехать, но то меньшее зло
        while (getPixelDiff(img1.getRGB(bXCenter, top),img2.getRGB(bXCenter, top)) > ignoreDiffLevel) {
            top++;
        }
        while (getPixelDiff(img1.getRGB(bXCenter, bottom),img2.getRGB(bXCenter, bottom)) > ignoreDiffLevel) {
            bottom--;
        }
        while (getPixelDiff(img1.getRGB(left, bYCenter),img2.getRGB(left, bYCenter)) > ignoreDiffLevel) {
            left++;
        }
        while (getPixelDiff(img1.getRGB(right, bYCenter),img2.getRGB(right, bYCenter)) > ignoreDiffLevel) {
            right--;
        }
        return new Rectangle(left,top,right-left,bottom-top);
    }

    private static final boolean isRectsColor(Robot rbt, ArrayList<Rectangle> rects, int color) throws IOException {
        Rectangle rect;
        Iterator<Rectangle> rectIter = rects.iterator();
        DataBuffer dBuf;
        int i;
        while (rectIter.hasNext()) {
            rect = rectIter.next();
            dBuf = rbt.createScreenCapture(rect).getData().getDataBuffer();
            for (i = 0; i < dBuf.getSize(); i++) {
                if (dBuf.getElem(i) != color) {
                    return false;
                }
            }
        }
        return true;
    }

    private static final boolean isAreaExact(Robot rbt, int x, int y, int width, int height, BufferedImage buttonImg) throws IOException {
        Rectangle rect = new Rectangle(x, y, width, height);
        BufferedImage areaScreen = rbt.createScreenCapture(rect);
        //ImageIO.write(areaScreen, "png", new File("D:\\test\\debug.png"));
        //System.out.println(isImgExact(areaScreen, buttonImg));
        return isImgExact(areaScreen, buttonImg);
    }

    private static WebDriver getBrowserDriver() {
        DesiredCapabilities dCaps = DesiredCapabilities.firefox();
        dCaps.setCapability("acceptSslCerts", true);
        dCaps.setCapability("nativeEvents", true);
        dCaps.setCapability("singleWindow", true);
        dCaps.setCapability("marionette", true);
        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile profile = allProfiles.getProfile("SeleniumFF");
        FirefoxBinary binary = new FirefoxBinary(new File("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"));
        driver = new FirefoxDriver(binary, profile, dCaps);
        return driver;
    }

    private static Robot getRobot() throws AWTException {
        robot = new Robot();
        return robot;
    }

    private static void getOffsets() {
        int bodyOffsetX = Math.toIntExact((Long) ((JavascriptExecutor) driver).executeScript("return (\n"
                + "function () {\n"
                + "	return window.mozInnerScreenX;\n"
                + "}\n"
                + ").call();"));
        int bodyOffsetY = Math.toIntExact((Long) ((JavascriptExecutor) driver).executeScript("return (\n"
                + "function () {\n"
                + "	return window.mozInnerScreenY;\n"
                + "}\n"
                + ").call();"));
        WebElement gameWindow = driver.findElement(By.id("flash_app"));
        gameWindowOffsetX = bodyOffsetX + gameWindow.getLocation().getX();
        gameWindowOffsetY = bodyOffsetY + gameWindow.getLocation().getY();
    }

    private static void openGameWindow() throws AWTException {
        getBrowserDriver();
        driver.get("http://vk.com/finddifference");
        driver.manage().window().maximize();
        getOffsets();
        getRobot();
    }

    static BufferedImage playBtnImg = null, stRoundImg = null;

    private static void clickStartBtn() throws IOException, TimeoutException, InterruptedException {
        final int playBtnOffsetX = 464, playBtnOffsetY = 464, playBtnSizeX = 40, playBtnSizeY = 50;
        if (playBtnImg == null) {
            playBtnImg = ImageIO.read(new File("D://play_btn.png"));
        }
        mouseMove(robot, gameWindowOffsetX, gameWindowOffsetY);
        Awaitility.await().atMost(60, SECONDS).with().pollInterval(500, TimeUnit.MILLISECONDS).until(() -> isAreaExact(robot, (gameWindowOffsetX + playBtnOffsetX), (gameWindowOffsetY + playBtnOffsetY), playBtnSizeX, playBtnSizeY, playBtnImg));
        moveAndClick(robot, (gameWindowOffsetX + playBtnOffsetX + playBtnSizeX / 2), (gameWindowOffsetY + playBtnOffsetY + playBtnSizeY / 2));
        waitForRound();
    }

    private static void waitForRound() throws IOException, InterruptedException {
        final int stRoundOffsetX = 471, stRoundOffsetY = 624, stRoundSizeX = 54, stRoundSizeY = 29;
        if (stRoundImg == null) {
            stRoundImg = ImageIO.read(new File("D://state_round.png"));
        }
        mouseMove(robot, gameWindowOffsetX, gameWindowOffsetY);
        Awaitility.await().atMost(30, SECONDS).with().pollInterval(500, TimeUnit.MILLISECONDS).until(() -> isAreaExact(robot, (gameWindowOffsetX + stRoundOffsetX), (gameWindowOffsetY + stRoundOffsetY), stRoundSizeX, stRoundSizeY, stRoundImg));
    }

    static final int diffImgsCount = 8;
    static BufferedImage[] diffImgs = null;

    private static int detectDiffCount() throws IOException {
        if (diffImgs == null) {
            diffImgs = new BufferedImage[diffImgsCount];
            for (int i = 0; i < diffImgsCount; i++) {
                diffImgs[i] = ImageIO.read(new File("D://diffcnt_" + (i + 1) + ".png"));
            }
        }
        Point diffRegSize = new Point(33, 34);
        Point[] diffRegOffsets = new Point[slvImgsCount];
        diffRegOffsets[7] = new Point(663, 526);
        diffRegOffsets[6] = new Point(637, 526);
        diffRegOffsets[5] = new Point(612, 526);
        diffRegOffsets[4] = new Point(586, 526);
        diffRegOffsets[3] = new Point(561, 526);
        diffRegOffsets[2] = new Point(535, 526);
        diffRegOffsets[1] = new Point(510, 526);
        diffRegOffsets[0] = new Point(484, 526);
        Rectangle diffRect = new Rectangle();
        BufferedImage diffArea;
        for (int i = diffImgsCount - 1; i >= 0; i--) {
            diffRect.setBounds(gameWindowOffsetX + diffRegOffsets[i].x, gameWindowOffsetY + diffRegOffsets[i].y, diffRegSize.x, diffRegSize.y);
            diffArea = robot.createScreenCapture(diffRect);
            if (isImgExact(diffArea, diffImgs[i])) {
                return (i + 1);
            }
        }
        return -1;
    }

    static final int slvImgsCount = 8;
    static BufferedImage[] slvImgs = null;
    private static int detectSolveCount(final int diffCount) throws IOException {
        if (slvImgs == null) {
            slvImgs = new BufferedImage[slvImgsCount];
            for (int i = 0; i < slvImgsCount; i++) {
                slvImgs[i] = ImageIO.read(new File("D://slvcnt_" + (i + 1) + ".png"));
            }
        }
        Point slvRegSize = new Point(33, 33);
        //Вот это плохо, что чекать
        Point[] slvRegBases = new Point[slvImgsCount];
        slvRegBases[7] = new Point(663, 525);
        slvRegBases[6] = new Point(637, 525);
        slvRegBases[5] = new Point(612, 525);
        slvRegBases[4] = new Point(586, 525);
        slvRegBases[3] = new Point(561, 525);
        slvRegBases[2] = new Point(535, 525);
        slvRegBases[1] = new Point(510, 525);
        slvRegBases[0] = new Point(484, 525);
        final int regStepSize = 51;
        Rectangle slvRect = new Rectangle();
        BufferedImage slvArea;
        for (int i = diffCount - 1; i >= 0; i--) {
            slvRect.setBounds(gameWindowOffsetX + slvRegBases[diffCount - 1].x - regStepSize * (diffCount - i - 1), gameWindowOffsetY + slvRegBases[diffCount - 1].y, slvRegSize.x, slvRegSize.y);
            slvArea = robot.createScreenCapture(slvRect);
            if (isImgExact(slvArea, slvImgs[i])) {
                return (i + 1);
            }
        }
        return 0;
    }

    static final int trImgsCount = 10;
    static BufferedImage[] trImgs = null;

    private static int detectTriesCount() throws IOException {
        Rectangle triesReg = new Rectangle(gameWindowOffsetX + 475, gameWindowOffsetY + 26, 50, 60);
        if (trImgs == null) {
            trImgs = new BufferedImage[trImgsCount];
            for (int i = 0; i < trImgsCount; i++) {
                trImgs[i] = ImageIO.read(new File("D://trycnt_" + (i + 1) + ".png"));
            }
        }
        BufferedImage triesArea = robot.createScreenCapture(triesReg);
        for (int i = trImgsCount - 1; i >= 0; i--) {
            if (isImgExact(triesArea, trImgs[i])) {
                return i + 1;
            }
        }
        return -1;
    }

    static ArrayList<Rectangle> imgRects = null;
    static BufferedImage checkWhiteImg = null;
    static ProcessorFitnessFunction pff = null;
    static Configuration conf = null;
    static Chromosome pS = null;
    static String imgNameBase;
    static String lastImgNameBase=null;
    private static ArrayList<Point> FindDifferences(int moreCount) throws Exception {
        if (imgRects == null) {
            imgRects = new ArrayList<>();
            imgRects.add(new Rectangle(gameWindowOffsetX + 44, gameWindowOffsetY + 123, 444, 392));
            imgRects.add(new Rectangle(gameWindowOffsetX + 511, gameWindowOffsetY + 123, 444, 392));
            checkWhiteImg = ImageIO.read(new File("D://empty_pic.png"));
        }
        final int picCheckOffsetX = 210, picCheckOffsetY = 280, picCheckWidth = 50, picCheckHeight = 50;
        Awaitility.await().atMost(60, SECONDS).with().pollInterval(500, TimeUnit.MILLISECONDS).until(() -> !isAreaExact(robot, (gameWindowOffsetX + picCheckOffsetX), (gameWindowOffsetY + picCheckOffsetY), picCheckWidth, picCheckHeight, checkWhiteImg));
        BufferedImage img1 = robot.createScreenCapture(imgRects.get(0)), img2 = robot.createScreenCapture(imgRects.get(1));
        if (lastImgNameBase != imgNameBase) {
            imgNameBase = ImageHasher.hashImage(img1, "md5");
            lastImgNameBase = imgNameBase;
            ImageIO.write(img1, "png", new File("D:\\diffpics\\" + imgNameBase + "_1.png"));
            ImageIO.write(img2, "png", new File("D:\\diffpics\\" + imgNameBase + "_2.png"));
        }
        int pCount = detectDiffCount() + moreCount;
        //System.out.println(pCount);
        if (pS == null) {
            pff = new ProcessorFitnessFunction("D:\\diffpics\\", "A:\\output\\");
            conf = ProcessorMutations.buildEvConf();
            pS = pff.deserializeGenes("A:\\output\\genes.json", conf);
        }
        return pff.getImagePoints(img1, img2, pS);
    }

    final static int solvePrice = 17;
    static BufferedImage panicImg = null;
    static boolean panicMode = false;

    private static void isPanic(int resource) throws IOException {
        int dCount = detectDiffCount();
        int sCount = detectSolveCount(dCount);
        int tCount = detectTriesCount();
         /*< 2 || (tCount <= (dCount - sCount)*/ 
        if (tCount< 2/* && ((dCount - sCount - resource / (float) solvePrice) > 0)*/) {
            System.out.println("Panic!");
            panicMode = true;
        }
    }

    static final Random r = new Random();
    static Rectangle[] detectedRects = null;
    static BufferedImage rectImg, prevRectImg;
    static final DBOperator dbOp = DBOperator.getInstance();
    private static void getSolveDiffs(final int solveCount, final int diffCount) {
        try 
            {
                    prevRectImg = rectImg;
                    final BufferedImage imgP = rectImg;
                    Awaitility.await().atMost(5, SECONDS).with().pollInterval(250, TimeUnit.MILLISECONDS).until(() -> !isImgExact(imgP, robot.createScreenCapture(imgRects.get(0))));
                    rectImg = robot.createScreenCapture(imgRects.get(0));
                    ImageIO.write(rectImg, "png", new File("D:\\test\\" + imgNameBase + "_slv_" + solveCount + "_" + diffCount + ".png"));
                    if (detectedRects != null) {
                        detectedRects[solveCount - 1] = getImgDiffRect(prevRectImg, rectImg);
                        if (solveCount == diffCount) {
                            
                            if (cheatsUsed) {
                                //tmp
                                Opener opener = new Opener();
                                ImagePlus imp = opener.openImage("D:\\test\\" + imgNameBase + "_slv_" + solveCount + "_" + diffCount + ".png");
                                for (Rectangle detectedRect : detectedRects) {
                                    //System.out.println(detectedRect.toString());
                                    dbOp.insertImageRect(detectedRect, imgNameBase);
                                    imp.setRoi(detectedRect.x,detectedRect.y,detectedRect.width,detectedRect.height);
                                    IJ.run(imp, "Variance...", "radius=4");
                                }
                                IJ.saveAs(imp, "PNG", "D:\\test\\" + imgNameBase + "_slv_" + solveCount + "_" + diffCount+"_highlight" + ".png");
                            }
                            
                            detectedRects = null;
                        }
                    }
            } catch (Exception e) {
        }
    }
    
    private static void clickDifferences(ArrayList<Point> points, int resource) throws InterruptedException, IOException {
        int i = 0;
        //BufferedImage img = robot.createScreenCapture(imgRects.get(0));
        rectImg = robot.createScreenCapture(imgRects.get(0));
        //prevRectImg = rectImg;
        final int diffCount = detectDiffCount();
        int solveCount = 0, prevSolveCount = 0;
        for (Point point : points) {
            isPanic(resource);
            if (panicMode) {
                return;
            }
            if (i == 0) {
                solveCount = detectSolveCount(diffCount);
                ImageIO.write(rectImg, "png", new File("D:\\test\\" + imgNameBase + "_slv_" + solveCount + "_" + diffCount + ".png"));
                prevSolveCount = solveCount;
                if (detectedRects == null && solveCount==0) {
                    detectedRects = new Rectangle[diffCount];
                }
            }
            Thread.sleep((long) (100 + r.nextFloat() * 200 * i));
            moveAndClick(robot, imgRects.get(0).x + point.x, imgRects.get(0).y + point.y);
            prevSolveCount = solveCount;
            final int sC = solveCount;
            try {
                Awaitility.await().atMost(3, SECONDS).with().pollInterval(250, TimeUnit.MILLISECONDS).until(() -> {
                    int slvCount = 0;
                    try {
                        slvCount = detectSolveCount(diffCount);
                    } catch (IOException ex) {
                        Logger.getLogger(Bot.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return sC != slvCount;
                });
            } catch (Exception e) {
            }
            Thread.sleep((long) (500 + r.nextFloat() * 250 * i));
            solveCount = detectSolveCount(diffCount);
            if (solveCount != prevSolveCount) {
                //System.out.println(solveCount+" : "+diffCount+" = "+(detectedRects!=null));
                getSolveDiffs(solveCount,diffCount);
            }
            i++;
        }
    }

    static BufferedImage nextLvlImg = null;
    private static void clickNextLevel() throws IOException, ConditionTimeoutException, InterruptedException {
        final int nextLvlOffsetX = 472, nextLvlOffsetY = 599, nextLvlSizeX = 72, nextLvlSizeY = 44;
        if (nextLvlImg == null) {
            nextLvlImg = ImageIO.read(new File("D:\\next_btn.png"));
        }
        mouseMove(robot, gameWindowOffsetX, gameWindowOffsetY);
        final int waitTime = panicMode ? 1 : 10;
        Awaitility.await().atMost(waitTime, SECONDS).pollInterval(250, TimeUnit.MILLISECONDS).until(() -> isAreaExact(robot, (gameWindowOffsetX + nextLvlOffsetX), (gameWindowOffsetY + nextLvlOffsetY), nextLvlSizeX, nextLvlSizeY, nextLvlImg));
        moveAndClick(robot, (gameWindowOffsetX + nextLvlOffsetX + nextLvlSizeX / 2), (gameWindowOffsetY + nextLvlOffsetY + nextLvlSizeY / 2));
        panicMode = false;
        waitForRound();
        lastImgNameBase=null;
        //Thread.sleep(1000);
    }

    static boolean cheatsUsed=false;
    //Юзает читы(если можно) и возвращает кол-во оставшихся ресурсов
    private static int useCheat(final int resources) throws IOException, Exception {
        int totalDiffs = detectDiffCount();
        int solvedDiffs = detectSolveCount(totalDiffs);
        int cheatsNeed = totalDiffs - solvedDiffs;
        if (cheatsNeed > resources / ((float) solvePrice)) {
            throw new Exception("Sorry, can't use cheats now.");
        }
        //Кликаем не глядя
        for (int i = 0; i < cheatsNeed; i++) {
            moveAndClick(robot, (gameWindowOffsetX + 142), (gameWindowOffsetY + 637));
            getSolveDiffs(solvedDiffs,totalDiffs);
            //Thread.sleep(2000);
        }
        cheatsUsed=true;
        return (resources - cheatsNeed * solvePrice);
    }

    private static void playSound(String path) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        File yourFile = new File(path);
        AudioInputStream stream;
        AudioFormat format;
        DataLine.Info info;
        Clip clip;
        stream = AudioSystem.getAudioInputStream(yourFile);
        format = stream.getFormat();
        info = new DataLine.Info(Clip.class, format);
        clip = (Clip) AudioSystem.getLine(info);
        clip.open(stream);
        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        gainControl.setValue(-20.0f); // Reduce volume by 10 decibels.
        clip.start();
    }

    public static void main2(String[] args) throws ParseException, SQLException, IOException, org.json.simple.parser.ParseException, ClassNotFoundException {
        //ProcessorFitnessFunction pff = new ProcessorFitnessFunction("D:\\diffpics\\", "A:\\output\\");
        //ArrayList<String> fileBaseNames = new ArrayList<>();
        //HashMap<String, ArrayList<Rectangle>> imgsRects = new HashMap<>();
        //dbOp.getImgsRects(fileBaseNames, imgsRects);
        try {
            openGameWindow();
            clickStartBtn();
            final int maxPics = 1000;
            final int maxMisses = 1;
            int misses = 0, imgsSolved = 20;
            ArrayList<Point> clickPoints;
            ArrayList<Point> prevClickPoints = new ArrayList<Point>();
            try {
                for (int currPics = 0; currPics < maxPics; currPics++) {
                    clickPoints = FindDifferences(misses);
                    //if (misses > 0) {
                        //System.out.print("Misses=" + misses + "; ClickPointsCountBefore=" + clickPoints.size());
                        /*
                        for (int i = 0; i < clickPoints.size(); i++) {
                            for (int j = 0; j < prevClickPoints.size(); j++) {
                                if (Math.abs(prevClickPoints.get(j).x - clickPoints.get(i).x) < 20 && Math.abs(prevClickPoints.get(j).y - clickPoints.get(i).y) < 20) {
                                    clickPoints.remove(i);
                                    i--;
                                    break;
                                }
                            }
                        }
                        */
                        //System.out.print("; ClickPointsCountAfter=" + clickPoints.size() + "\n");
                    //}
                    clickDifferences(clickPoints, imgsSolved);
                    try {
                        //TODO: Очень топорный вэйт. Может лучше по чекнутым диффам проверять?
                        clickNextLevel();
                        panicMode = false;
                        prevClickPoints.clear();
                        playSound("D:\\item.wav");
                        imgsSolved++;
                        misses = 0;
                    } catch (ConditionTimeoutException ex) {
                        if (misses < maxMisses) {
                            prevClickPoints.addAll(clickPoints);
                            misses++;
                            currPics--;
                        } else {
                            System.out.println("Trying to use cheat");
                            imgsSolved = useCheat(imgsSolved);
                            panicMode = false;
                            cheatsUsed=false;
                            prevClickPoints.clear();
                            clickNextLevel();
                            playSound("D:\\item.wav");
                            imgsSolved++;
                            misses = 0;
                            //"Throw ex" - звучит иронично.
                            //throw ex;
                        }
                    }
                }
            } catch (Exception e) {
                //playSound("D:\\lose.wav");
                driver.manage().window().setSize(new Dimension(1200, 300));
                System.out.println(e.getMessage());
                throw e;
            }
            playSound("D:\\win.wav");
            driver.manage().window().setSize(new Dimension(600, 400));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
